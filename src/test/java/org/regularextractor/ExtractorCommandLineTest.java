package org.regularextractor;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URISyntaxException;
import java.net.URL;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.regularextractor.RegularExtractor;

/**
 * Tests the command line
 * @author Matt
 *
 */
public class ExtractorCommandLineTest {
	
	PrintStream out;
	ByteArrayOutputStream bytes;
	RegularExtractor regularExtractor;
	
	
	@Before
	public void setup() {
		
		bytes = new ByteArrayOutputStream();
		out = new PrintStream(bytes);
		regularExtractor = new RegularExtractor();
		regularExtractor.setOutputStream(out);
	}
	
	@After
	public void cleanup() throws IOException {
		out.close();
	}
	/**
	 * Used for command line tests
	 * @return
	 */
	private String out() {
		return new String(bytes.toByteArray()).trim();
	}
	
	@Test
	public void test_cli() throws URISyntaxException {
		
		URL sourcesFile = getClass().getResource("./sources.txt");
		URL regexesFile = getClass().getResource("./regexes.txt");
		
		String[] args = {"-sources", sourcesFile.toURI().toString() , "-regexes", 
				regexesFile.toURI().toString()};
		regularExtractor.init(args);		
		assertEquals("http://www.example.com,Example Domain", out());
		
	}
	
}
