package org.regularextractor.regex;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

/**
 * Tests the extractor
 * 
 * @author Matt
 *
 */
public class ExtractorTest {

	@SuppressWarnings("unused")
	@Test
	public void test_nulls() {
		
		List<String> one = new ArrayList<>();
		one.add("[!");
		
		try {
			Extractor e = new Extractor(null, null);
			fail("Expected exception");
		} catch (ExtractorException ee) {
			// fine
		}
		
		try {
			Extractor e = new Extractor(one, null);
			fail("Expected exception");
		} catch (ExtractorException ee) {
			// fine
		}
		
		try {
			Extractor e = new Extractor(null, one);
			fail("Expected exception");
		} catch (ExtractorException ee) {
			// fine
		}
		
		try {
			Extractor e = new Extractor(one, one);
			fail("Expected exception");
		} catch (ExtractorException ee) {
			// fine
		}
	}
	
	@Test
	public void test_customRegex() throws URISyntaxException, ExtractorException {
		List<String> regexes = new ArrayList<>();
		String titleRegex = "<title>([0-9a-zA-Z _]*)</title>";
		regexes.add("UK-POSTCODE");
		regexes.add(titleRegex);
		regexes.add("US-ZIPCODE");
		
		
		List<String> sources = new ArrayList<>();
		
		URL resourceUrl = getClass().getResource("./sample.html");
		//Path resourcePath = Paths.get(resourceUrl.toURI());
		
		sources.add(resourceUrl.toURI().toString());
		
		Extractor extractor = new Extractor(sources, regexes);
		extractor.run();
		
		List<ExtractionResult> results = new ArrayList<ExtractionResult>();
		results.addAll(extractor.getResults().values());
		
		assertTrue(results != null);
		assertTrue(results.size() > 0);
		
		ExtractionResult sample = results.get(0);
		List<List<String>> tree = sample.getResults();
		

		assertEquals("SW1A 2AA", tree.get(0).get(0));
		assertEquals("This is a page", tree.get(1).get(0));
		assertEquals("20500", tree.get(2).get(0));
	}
	
	@Test
	public void test_http() throws URISyntaxException, ExtractorException {
		List<String> regexes = new ArrayList<>();
		String titleRegex = "<title>([0-9a-zA-Z _]*)</title>";
		regexes.add(titleRegex);
		regexes.add("DOMAIN");

		
		
		List<String> sources = new ArrayList<>();
	
		
		sources.add("http://www.example.com");
		
		Extractor extractor = new Extractor(sources, regexes);
		extractor.run();
		
		List<ExtractionResult> results = new ArrayList<ExtractionResult>();
		results.addAll(extractor.getResults().values());
		
		assertTrue(results != null);
		assertTrue(results.size() > 0);
		
		ExtractionResult sample = results.get(0);
		List<List<String>> tree = sample.getResults();
		
		assertEquals("Example Domain", tree.get(0).get(0));
		assertEquals("www.iana.org", tree.get(1).get(0));
	}
	
	@Test
	public void test_samples() throws ExtractorException, URISyntaxException {
		
		List<String> regexes = new ArrayList<>();
		regexes.add("UK-POSTCODE");	//0
		regexes.add("US-ZIPCODE");	//1
		regexes.add("DOMAIN");//2
		regexes.add("IPV4");//3
		regexes.add("ISBN10");//4
		regexes.add("ISBN13");//5
		regexes.add("UK-PHONE");//6
		regexes.add("US-PHONE");//7
		regexes.add("HTML-CSS");//8
		regexes.add("HTML-SCRIPTS");//9
		regexes.add("HTML-IMAGES");//10
		regexes.add("HTML-TITLE");//11
		
		List<String> sources = new ArrayList<>();
		
		URL resourceUrl = getClass().getResource("./sample.html");
		//Path resourcePath = Paths.get(resourceUrl.toURI());
		
		sources.add(resourceUrl.toURI().toString());
		
		Extractor extractor = new Extractor(sources, regexes);
		extractor.run();
		
		List<ExtractionResult> results = new ArrayList<ExtractionResult>();
		results.addAll(extractor.getResults().values());
		
		assertTrue(results != null);
		assertTrue(results.size() > 0);
		
		ExtractionResult sample = results.get(0);
		List<List<String>> tree = sample.getResults();
		

		assertEquals("SW1A 2AA", tree.get(0).get(0));
		assertEquals("20500", tree.get(1).get(0));
		
		List<String> domains = tree.get(2);
		assertEquals(7, domains.size());
		assertEquals("www.example1.com", domains.get(0));
		assertEquals("www.example2.com", domains.get(1));
		assertEquals("example3.com", domains.get(2));
		assertEquals("www.example4.com", domains.get(3));
		assertEquals("ftp.example5.com", domains.get(4));
		assertEquals("example6.com", domains.get(5));
		assertEquals("example7.com", domains.get(6));
		
		assertEquals("10.0.0.1", tree.get(3).get(0));
		
		List<String> isbn10s = tree.get(4);
		assertEquals("979076533X", isbn10s.get(0));
		assertEquals("1409145719", isbn10s.get(1));

		List<String> isbn13s = tree.get(5);
		assertEquals("9790765335999", isbn13s.get(0));
		assertEquals("978-1409145714", isbn13s.get(1));
		
		assertEquals("020 7123 4567", tree.get(6).get(0));
		assertEquals("(01234) 567 890", tree.get(6).get(1));
		assertEquals("01201 55 55 55", tree.get(6).get(2));
		
		assertEquals("234-235-5678", tree.get(7).get(0));
		assertEquals("1 650-253-0000", tree.get(7).get(1));
		
		assertEquals("This is a page", tree.get(11).get(0));
		assertEquals("http://www.example1.com/script.js", tree.get(9).get(0));
		assertEquals("header.jpg", tree.get(10).get(0));
		assertEquals("spacer.png", tree.get(10).get(1));
		assertEquals("style.css", tree.get(8).get(0));
		
	}
	
}
