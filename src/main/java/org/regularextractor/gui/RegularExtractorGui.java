package org.regularextractor.gui;

import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TitledPane;
import javafx.stage.Stage;
import javafx.util.Callback;

import org.regularextractor.regex.ExtractionResult;
import org.regularextractor.regex.Extractor;
import org.regularextractor.regex.ExtractorException;

/**
 * Main GUI for regular extractor
 * 
 * @author Matt
 *
 */
public class RegularExtractorGui implements Initializable {

	/** Default logger */
	private static Logger log  = Logger.getLogger(RegularExtractorGui.class.getName());

	
	/** Bindings for controls */
	@FXML Parent root;
	@FXML private MenuItem loadFile;
	@FXML private MenuItem reset;
	@FXML private MenuItem close;
	@FXML private MenuItem about;
	
	// step 1
	@FXML private TitledPane step1;
	@FXML private TextArea sources;
	
	// step 2
	@FXML private TitledPane step2;
	@FXML private Button insertButton;
	@FXML private TextArea regexes;
	
	// step 3
	@FXML private TitledPane step3;
	@FXML private Button runButton;
	@FXML private Button stopButton;
	@FXML private ProgressBar progress;
	@FXML private Label doneSoFarLabel;
	@FXML private Label totalToDoLabel;

	@FXML private TextArea csvOutput;
	@FXML private TableView<ObservableList<String>> resultsTable
	;
	//@FXML private TableColumn<OutputModel, String> fileColumn;
	//@FXML private TableColumn<OutputModel, String> functionColumn;
	//@FXML private TableColumn<OutputModel, String> hashColumn;

	/** Task used for background procesing */
	private Service<Void> task;
	
	/** Stage */
	@SuppressWarnings("unused")
	private Stage stage;
	
	/**
	 * Called by JavaFX when initialisation of this controller is complete
	 */
	public void initialize(URL fxmlFileLocation, ResourceBundle resource) {
		
		setupBinding();	
		
		
	}
		
	/**
	 * Set the stage and initialise Drag & drop
	 */
	public void setStage(Stage stage) {
		this.stage = stage;
		
	}
	
	/**
	 * Setup binding to the model
	 */
	private void setupBinding() {
      //  model.md2.bind(md2.selectedProperty());
      //  model.md4.bind(md4.selectedProperty());
      //  model.md5.bind(md5.selectedProperty());
        
        
	}
	
	/**
	 * Close menu
	 * 
	 * @param event
	 */
	@FXML protected void handleCloseMenuAction(ActionEvent event) {
		System.exit(0);
	}

	/**
	 * Insert from library button
	 * 
	 * @param event
	 */
	@FXML protected void handleInsertButtonAction(ActionEvent event) {
		
	}

	/**
	 * run button
	 * 
	 * @param event
	 */
	@FXML protected void handleStopButtonAction(ActionEvent event) {
		progress.progressProperty().unbind();
		progress.setProgress(1);
		if (task != null) {
			if (task.isRunning()) {				
				task.cancel();
			}
		}
	}
	
	/**
	 * Update the result list.  
	 * @param result
	 */
	private void updateResults(ExtractionResult result) {
		
		
		
		Platform.runLater(() -> {
			// text
			StringBuilder str = new StringBuilder();
			str.append(result.getSource());
			result.getResults().stream()
				.forEach((List<String> list) -> {
					list.forEach((String listItem) -> {
						str.append(",");
						str.append(listItem);
					});
				});
			str.append("\n");
			csvOutput.appendText(str.toString());
			
			// table
			ObservableList<String> row = FXCollections.observableArrayList();
			row.addAll(result.getSource());
			row.addAll(result.getStatus());
			
			StringBuilder str2;
									
			for (List<String> list : result.getResults()) {
				
				//sort for ease of use
				list.sort(null);
				
				str2 = new StringBuilder();
				for (String listItem : list) {
					str2.append(listItem);
					str2.append("\n");
				}
				row.addAll(str2.toString());				
			}
			
			resultsTable.getItems().add(row);
			
		});
	}
	
	/**
	 * Resets the table and adds in the appropriate columns
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void resetTable(List<String> regexList) {
		resultsTable.getItems().clear();
		resultsTable.getColumns().clear();

		//Source & status column first
		TableColumn c = new TableColumn("Source");
		c.setCellValueFactory(new Callback<CellDataFeatures<ObservableList, String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(
					CellDataFeatures<ObservableList, String> param) {
				return new SimpleStringProperty(param.getValue().get(0).toString());
			}
		});
		resultsTable.getColumns().addAll(c);
		c = new TableColumn("Status");
		c.setCellValueFactory(new Callback<CellDataFeatures<ObservableList, String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(
					CellDataFeatures<ObservableList, String> param) {
				return new SimpleStringProperty(param.getValue().get(1).toString());
			}
		});
		resultsTable.getColumns().addAll(c);
		
		for (int i=0; i<regexList.size();i++) {
			final int j = i;
			c = new TableColumn(regexList.get(i));
			c.setCellValueFactory(new Callback<CellDataFeatures<ObservableList, String>, ObservableValue<String>>() {

				@Override
				public ObservableValue<String> call(
						CellDataFeatures<ObservableList, String> param) {
					int j2 = j + 2;
					if (param.getValue().size() > j2) {
						return new SimpleStringProperty(param.getValue().get(j2).toString());
					} else {
						return new SimpleStringProperty("<UNKNOWN>");
					}
				}
			});
			resultsTable.getColumns().addAll(c);
		}
	}
	
	/**
	 * run button
	 * 
	 * @param event
	 */

	@FXML protected void handleRunButtonAction(ActionEvent event) {

		final List<String> sourcesList = Arrays.asList(sources.getText().split("\\n"));
		final List<String> regexList = Arrays.asList(regexes.getText().split("\\n"));
		
		// clear output
		csvOutput.clear();
		resetTable(regexList);
		

		int countSources = sourcesList.size();
		int countRegex = regexList.size();
		int max = countRegex * countSources;
		totalToDoLabel.setText(String.valueOf(max));

		task = new Service<Void>() {
		    @Override
		    protected Task<Void> createTask() {
		        return new Task<Void>() {
				    @Override         		  
				    public Void call() {
				    	
				    	Extractor extractor;
				        try {
					    	extractor = new Extractor(sourcesList, regexList);
					    	extractor.run((Integer progress) -> {
					    		if (isCancelled()) {
				                     return false;
				                 }
					    		updateProgress(progress, max);
					    		Platform.runLater(() -> {
					    			doneSoFarLabel.setText(String.valueOf(progress));
					    		});
					    		return true;
					    	},(result) -> {					    	
					    		updateResults(result);
					    	});
					    	
				        } catch (ExtractorException ee) {
				        	log.warning("Unexpecting extraction error: " + ee.getMessage());
				        }
				    	
				        // At this point everything should have returned one way or another.
						progress.progressProperty().unbind();
						progress.setProgress(1);
						
				        return null;
				        
				    }
				};
		    }
		};
		
		progress.progressProperty().unbind();
        progress.progressProperty().bind(task.progressProperty());

        task.start();
		
	}

}
