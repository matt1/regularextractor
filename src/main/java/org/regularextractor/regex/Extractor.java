package org.regularextractor.regex;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.commons.io.IOUtils;

/**
 * Extracts values from sources based on list of input and list of regexs
 * 
 * @author Matt
 *
 */
public class Extractor implements Runnable {

	/** Default logger */
	private static Logger log  = Logger.getLogger(Extractor.class.getName());
	
	/** Sources to extract from */
	private List<String> sources;
	
	/** Regexes to use */
	private List<Pattern> regexes;
	
	/** Results from the extraction */
	//private List<ExtractionResult> results;
	private ConcurrentHashMap<String, ExtractionResult> results;
	
	/**
	 * Creates a new extractor that will process the resources and extract the values based on the
	 * regex
	 * 
	 * @param sources
	 * @param regex
	 * @throws ExtractorException 
	 */
	public Extractor(List<String> sources, List<String> regexes) throws ExtractorException {
		
		if (sources == null || sources.size() < 1) {
			throw new ExtractorException("No sources were provided for extraction");
		}
		
		if (regexes == null || regexes.size() < 1) {
			throw new ExtractorException("No regular expressions were specified to extract data.");
		}
		
		this.regexes = new ArrayList<Pattern>(regexes.size());
		
		for (String regex : regexes) {
			String toCompile = regex.trim();
			// Check for known regexes
			CommonRegex commonRegex = ExtractorUtils.getCommonRegex(toCompile);
			if (commonRegex != null) {
				toCompile = commonRegex.getRegex();
			}
			try {
				Pattern p = Pattern.compile(toCompile);
				this.regexes.add(p);
			} catch (PatternSyntaxException pse) {
				throw new ExtractorException("Regular expression '" + toCompile + "' has " +
						" incorrect syntax.",	pse);
			}
		}

		this.sources = sources;
		this.results = new ConcurrentHashMap<>(sources.size());
	}

	/**
	 * Get the results of this extraction
	 * 
	 * @return
	 */
	public ConcurrentHashMap<String, ExtractionResult> getResults() {
		return this.results;
	}
	
	/**
	 * Process the request without a progress callback
	 */
	public void run() {
		run((a)->{
			// Don't cancel, just keep on going
			return true;
		},(a)->{
			// Nothing in result handler
		});
		
	}
	
	/**
	 * Class to do actual site by site extraction
	 * @author Matt
	 *
	 */
	private class ExtractorWorker implements Callable<ExtractionResult> {

		private String source;
		private ConcurrentHashMap<String, ExtractionResult> results;
		private AtomicInteger progressCounter;
		Predicate<Integer> updateFunction;
		
		/**
		 * Create a new extractor worker
		 * @param source
		 * @param results
		 * @param updateFunction 
		 * @param current 
		 */
		public ExtractorWorker(String source, ConcurrentHashMap<String, ExtractionResult> results, 
				AtomicInteger current, Predicate<Integer> updateFunction) {
			this.source = source.trim();
			this.results = results;
			this.progressCounter = current;
			this.updateFunction = updateFunction;
			
		}
		
		@Override
		public ExtractionResult call() throws Exception {
			ExtractionResult sourceResult = new ExtractionResult(source);
			
			// Get the file as a String
			String sourceString;
			try {
				// Check for local files first
				File f = new File(source);
				if (f.exists()) {
					source = f.toURI().toURL().toString();
				}										
				URLConnection urlcon = new URL(source).openConnection();
				urlcon.addRequestProperty("User-Agent", ExtractorUtils.getUserAgent());
				InputStream in = urlcon.getInputStream();
				
				sourceString = IOUtils.toString( in );
				
				
				// Then match each pattern to to the string
				for (Pattern pattern : regexes) {
				
					Matcher matcher = pattern.matcher(sourceString);
					List<String> matches = new Vector<>();
					String matchResult = "";
					while (matcher.find()) {
						if (matcher.groupCount() >= 1) {
							matchResult = matcher.group(1);
						} else {
							matchResult = matcher.group();
						}
						
						if (!matches.contains(matchResult)) {
							matches.add(matchResult);
						}
						
					}
					// report progress
					updateFunction.test(progressCounter.incrementAndGet());
					
					// add results
					sourceResult.addResult(matches);			

				};
				sourceString = null;
				in.close();
				//}
				sourceResult.setStatus("Ok");
				// Store this source's results in full results
				if (sourceResult.getResults().size() > 0) {
					this.results.put(source, sourceResult);
				}
				
			} catch (MalformedURLException mue) {
				log.warning("Malformed URL: " + mue.getMessage());
				sourceResult.setStatus("URL Error");
			} catch (IOException ioe) {
				log.warning("Error oepning file/stream: " + ioe.getMessage());
				sourceResult.setStatus("IO Error");
			}
			return sourceResult;
		}


		
	}
	
	/**
	 * Process the request
	 * 
	 * @param updateFunction Callback to report current progress count
	 */
	public void run(Predicate<Integer> updateFunction, Consumer<ExtractionResult> resultFunction) {

		// Time to wait per request (seconds)
		int threadTimeout = 10;
		// How long to wait until resizing the thread pool downwards (seconds)
		int threadIdleTimeout = 10;
		
		int initialThreads = 4;
		int maxThreads = 32;
		
		AtomicInteger current = new AtomicInteger(0);
		
		Collection<Future<ExtractionResult>> futures = 
				Collections.synchronizedList(new LinkedList<Future<ExtractionResult>>());
		ThreadPoolExecutor executor = 
				new ThreadPoolExecutor(initialThreads, maxThreads, threadIdleTimeout, 
						TimeUnit.SECONDS, new ArrayBlockingQueue<>(sources.size()));
		
		for (String source : sources) {
			futures.add(
					executor.submit(new ExtractorWorker(source, results, current, updateFunction)));			
		}
		
		// wait for all futures to complete
		for (Future<ExtractionResult> future : futures) {
		    try {
		    	boolean c = updateFunction.test(current.get());
		    	if (!c) {
		    		future.cancel(true);
		    	}
				ExtractionResult result = future.get(threadTimeout,TimeUnit.SECONDS);
				resultFunction.accept(result);
			} catch (InterruptedException e) {
				log.warning("Interuptted: " + e.getMessage());
			} catch (ExecutionException e) {
				log.warning("Execution exception: " + e.getMessage());
			} catch (TimeoutException e) {
				log.warning("Timed-out: " + e.getMessage());
			}
		}

	}
	
}
