package org.regularextractor.regex;

import java.util.ArrayList;
import java.util.List;

/**
 * Stores results from an extraction
 * 
 * @author Matt
 *
 */
public class ExtractionResult {

	/** Path or URL from where we did the extraction */
	private String source;
	
	/** Status of this source (e.g. success, failure) */
	private String status;
	
	/** Contains the map of regex to string results*/
	private List<List<String>> results = new ArrayList<List<String>>();
	
	public ExtractionResult(String source) {
		this.source = source;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getStatus() {
		return this.status;
	}
	
	/**
	 * Add a result to the result map
	 * @param regex Regex used to get value
	 * @param result List of values for the regular expression
	 */
	public void addResult(List<String> result) {
		this.results.add(result);
	}
	
	public List<List<String>> getResults() {
		return results;
	}
	
	public String getSource() {
		return source;
	}
}
