package org.regularextractor.regex;

import java.util.HashMap;
import java.util.Random;


/**
 * Utils
 * @author Matt
 *
 */
public class ExtractorUtils {

	/** Hashmap for common regexes */
	private static HashMap<String, CommonRegex> commonRegexes = new HashMap<>();
	static {
		commonRegexes.put("DOMAIN", CommonRegex.DOMAIN);
		commonRegexes.put("IPV4", CommonRegex.IPV4);
		commonRegexes.put("ISBN10", CommonRegex.ISBN10);
		commonRegexes.put("ISBN13", CommonRegex.ISBN13);
		commonRegexes.put("US-PHONE", CommonRegex.US_PHONE);
		commonRegexes.put("US-ZIPCODE", CommonRegex.US_ZIPCODE);
		commonRegexes.put("UK-PHONE", CommonRegex.UK_PHONE);
		commonRegexes.put("UK-POSTCODE", CommonRegex.UK_POSTCODE);
		commonRegexes.put("HTML-CSS", CommonRegex.HTML_CSS);
		commonRegexes.put("HTML-IMAGES", CommonRegex.HTML_IMAGES);
		commonRegexes.put("HTML-SCRIPTS", CommonRegex.HTML_SCRIPT);
		commonRegexes.put("HTML-TITLE", CommonRegex.HTML_TITLE);
		
		
	}
	
	/** Random for altering the user agent */
	private static Random random = new Random(System.currentTimeMillis());
	
	/**
	 * Gets a common built-in regex by name if found, othrtwise null
	 * 
	 * @param name
	 * @return
	 */
	public static CommonRegex getCommonRegex(String name) {
		if (commonRegexes.containsKey(name.toUpperCase())) {
			return commonRegexes.get(name.toUpperCase());
		}
		return null;
	}
	
	/**
	 * Gets a "real" user agent - Might get access by mulktiple workers at once but we're not
	 * synchronizing ... it will just add to the entropy :-)
	 * @return
	 */
	public static String getUserAgent() {
		
		int ran1 = random.nextInt(9 - 0 + 1) + 9;
		int ran2 = random.nextInt(99 - 0 + 1) + 99;
		int ran3 = random.nextInt(999 - 0 + 1) + 999;
		int ran4 = random.nextInt(9999 - 0 + 1) + 9999;
		return "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37." + String.valueOf(ran1) + "." + String.valueOf(ran4) + "." + String.valueOf(ran3) + " Safari/537." + String.valueOf(ran2);
	}
}
