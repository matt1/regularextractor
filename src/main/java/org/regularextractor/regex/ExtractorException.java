package org.regularextractor.regex;

/**
 * Exceptions from the extractor
 * 
 * @author Matt
 *
 */
public class ExtractorException extends Exception {

	private static final long serialVersionUID = 8778173489074522566L;

	
	public ExtractorException(String message) {
		super(message);
	}
	
	public ExtractorException(String message, Throwable cause) {
		super(message, cause);
	}
}
