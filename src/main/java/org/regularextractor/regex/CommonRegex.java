package org.regularextractor.regex;

/**
 * Common regular expressions
 * 
 * @author Matt
 *
 */
public enum CommonRegex {

	ADVANCED_DOMAIN("Advanced domain names - does not match a known list of TLDs, but could match anything 'domain shaped'.","Full domain name (using any TLD) or domain-shaped, e.g. www.example.cc or window.document.dir", "(?://|@|\\s)((?:(?!\\d+\\.)[a-zA-Z0-9_\\-]{1,63}\\.)+(?:[a-zA-Z]{2,}))"),
	DOMAIN("Domain names using common TLDs like .com, .co.uk etc","Full domain name, e.g. www.example.com", "((?:(?!\\d+\\.)[a-zA-Z0-9_\\-]{1,63}\\.)+(?:com|org|net|edu|gov|info|io|au|ch|cn|de|es|fr|it|jp(?!g|eg)|nl|no|ru|se|uk|us))"),
	
	HTML_IMAGES("Page Images - image file names from the current page's <img> tags that are in common formats (png, gif etc).","Image paths, e.g. picture1.png, http://site.com/folder/pic-1.jpeg","src=(?:'|\")?((?:http(s)?://)?[a-zA-Z/\\-_0-9\\.]*(png|jpg|jpeg|bmp|ico|gif|svg|tga|tif|tiff))(?:'|\")?"),
	HTML_SCRIPT("Page Scripts - script file names from the current page's <script> tags.", "Script paths, e.g. script1.js, //remoteserver.com/script.js","src=(?:'|\")?((?:http(s)?://)?[a-zA-Z/\\-_0-9\\.]*(js))(?:'|\")?"),
	HTML_CSS("Page Stylesheets - stylesheet filenames from the current page's <link> tags.", "CSS stylesheet paths, e.g. style.css", "href=(?:'|\")?((?:http(s)?://)?[a-zA-Z/\\-_0-9\\.]*(css))(?:'|\")?"),
	HTML_TITLE("Page Title - the title of the page using letters, numbers, spaces and dashes.", "A normal title, e.g. www.example.com's is 'Example Domain'","<(?:title|TITLE)>([\\w\\d \\-,!?\\.:;'\"|<>\\(\\)���&]*)</"),
	
	IPV4("IPv4 address", "Valid IP addresses, e.g. 127.0.0.1", "\\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\b"),
	
	ISBN10("ISBN-10", "10 digit ISBN numbers, with or without ISBN/ISBN-10 prefix.", "ISBN(?:-10)?:?\\ *((?=\\d{1,5}([ -]?)\\d{1,7}\\2?\\d{1,6}\\2?\\d)(?:\\d\\2*){9}[\\dX])(?![0-9])"),
	ISBN13("ISBN-13", "13 digit ISBN numbers, with or without ISBN/ISBN-13 prefix.", "ISBN(?:-13)?:?\\ *(97(?:8|9)([ -]?)(?=\\d{1,5}\\2?\\d{1,7}\\2?\\d{1,6}\\2?\\d)(?:\\d\\2*){9}\\d)"),
	
	UK_PHONE("UK Phone Number", "UK phone number, e.g. +447946123456 or 020 7123 4567","((\\+44\\d{4}(\\s){0,1}(\\d{6}|\\d{3}\\s\\d{3}|\\d{2}\\s\\d{2}\\s\\d{2}))|(\\(?0\\d{4}\\)?\\s?(\\d{3}\\s?\\d{3}|\\d{2}\\s?\\d{2}\\s?\\d{2}))|(\\(?0\\d{3}\\)?\\s?\\d{3}\\s?\\d{4})|(\\(?0\\d{2}\\)?\\s?\\d{4}\\s?\\d{4}))(\\s?\\#(\\d{4}|\\d{3}))?"),
	UK_POSTCODE("UK Post codes", "Full UK post codes, e.g. SW1A 2AA", "(([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])))) {0,1}[0-9][A-Za-z]{2}))"),	
	
	US_PHONE("US Phone Number", "US phone number in NANP format, e.g. 234-235-5678 or 1 (234) 567-8901","((?:1(?:-|\\s))?(?:\\(?[2-9]\\d{2}\\)?)(?:-|\\s)(?:[2-9]\\d{2})(?:-|\\s)(?:\\d{4}))"),
	US_ZIPCODE("US Zip Codes", "Numeric US Zipcodes, e.g. 20500", "(\\d{5}(-\\d{4})?)");
	
	private String regex;
	private String description;
	private String name;
	
	private CommonRegex(String name, String description, String regex) {
		this.name = name;
		this.regex = regex;
		this.description = description;
	}
	
	/**
	 * Get the name of the regex
	 * 
	 * @return
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Get the description of the regex
	 * 
	 * @return
	 */
	public String getDescription() {
		return this.description;
	}
	
	/**
	 * Get the actual regular expression
	 * 
	 * @return
	 */
	public String getRegex() {
		return regex;
	}
	
}
