package org.regularextractor;

import java.io.IOException;
import java.util.logging.Logger;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import org.regularextractor.gui.RegularExtractorGui;


/**
 * JavaFX Application for the GUI app.  
 * 
 * @author matt
 *
 */
public class RegularExtractorApplication extends Application {
	
	/** Default logger */
	private static Logger log  = Logger.getLogger(RegularExtractorApplication.class.getName());
	
	@Override
	public void start(Stage stage) throws IOException {
		log.info("Starting Application...");
		
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("./gui/RegularExtractorGui.fxml"));
        Parent root = fxmlLoader.load();
        RegularExtractorGui controller = (RegularExtractorGui) fxmlLoader.getController();
        fxmlLoader.setRoot(root);
        
		
        Scene scene = new Scene(root);		     
        stage.setTitle("Regular Extractor");
        stage.setScene(scene);
        stage.show();
        
        controller.setStage(stage);
	}

	public static void main(String[] args) {
		launch(args);
	}

}





