package org.regularextractor;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import org.apache.commons.io.IOUtils;
import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.regularextractor.regex.ExtractionResult;
import org.regularextractor.regex.Extractor;
import org.regularextractor.regex.ExtractorException;

/**
 * Creates a new regular extractor
 * 
 * @author Matt
 *
 */
public class RegularExtractor {

	/** Default output stream to use for output */
	PrintStream output = System.out;
	
	/** Default logger */
	private static Logger log  = Logger.getLogger(RegularExtractor.class.getName());

	/**
	 * Contains all of the command line options for use with args4j
	 * @author matt
	 *
	 */
	private class CommandOptions {
		
		@Option(name="-sources", usage="File containing sources (one per line)")
		private String sourcesFile;

		@Option(name="-regexes", usage="File containing regexes to use (one per line)")
		private String regexesFile;
		
		@Argument
		private List<String> arguments = new ArrayList<String>();

		public String getSourcesFile() {
			return sourcesFile;
		}

		public String getRegexesFile() {
			return regexesFile;
		}
				
	}
	
	/**
	 * Create a new regular extractor and process the arguments
	 * 
	 * @param arguments
	 */
	public static void main(String[] arguments) {
		RegularExtractor regularExtractor = new RegularExtractor();
		regularExtractor.init(arguments);
	}
	
	/**
	 * Initialise the application - start by processing the command line arguments
	 * 
	 * @param arguments
	 */
	public void init(String[] arguments) {
		
		// try parsing the command line to see if we got anything
		CommandOptions commandOptions = new CommandOptions();
		CmdLineParser commandParser = new CmdLineParser(commandOptions);
		
		try {
			commandParser.parseArgument(arguments);							
				
			// Check if we're in commandline mode or not
			if (null != commandOptions.getSourcesFile() && 
				null != commandOptions.getRegexesFile()) {
				noGui(commandOptions);				
			} else {
				gui(arguments);
			}
			
		} catch (CmdLineException cle) {
			log.info("Bad arguments!  Leave blank to start GUI, or:");
			commandParser.printUsage(System.err);
		}
	}
	
	/**
	 * Kick off the GUI
	 */
	private void gui(String[] arguments) {
		RegularExtractorApplication.launch(RegularExtractorApplication.class, arguments);
	}
	
	/**
	 * Process the input in command line mode
	 * 
	 * @param commandOptions
	 */
	private void noGui(CommandOptions commandOptions) {
		try {
			// Command line/batch implementation		
			//List<String> args = commandOptions.getArguments();
			
			// Read in both sources and regexes and start processing
			List<String> sources = loadInput(commandOptions.getSourcesFile());
			List<String> regexes = loadInput(commandOptions.getRegexesFile());
			
			Extractor extractor = new Extractor(sources, regexes);
			extractor.run();
			
			extractor.getResults().values().stream().forEach((ExtractionResult result) -> {
				
				StringBuilder str = new StringBuilder();
				str.append(result.getSource());
				result.getResults().stream().forEach((List<String> list) -> {
					list.forEach((String listItem) -> {
						str.append(",");
						str.append(listItem);
					});
				});
				output.println(str.toString());
				
			});
			
			
			
			
		} catch (IOException e) {
			log.severe("IO Exception trying to load input file: " + e.getMessage());
			System.exit(1);
		} catch (ExtractorException e) {
			log.severe("Unrecoverable error when processing: " + e.getMessage());
			System.exit(1);
		}
	}
	
	/**
	 * Load the file and put each row into a list of strings
	 * @param path
	 * @return
	 * @throws IOException 
	 */
	private List<String> loadInput(String path) throws IOException {
		
		InputStream in = new URL(path).openStream();
		String allLines= IOUtils.toString(in);
		
		return Arrays.asList(allLines.split("\\n"));
		    
	}
	
	/**
	 * Set an output stream for use by the default output
	 * 
	 * @param out
	 */
	public void setOutputStream(PrintStream out) {
		output = out;
	}
	
}
